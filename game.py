from random import randint

name = input("Hi! what is your name? ")

# guess 1
for guess_number in range(1, 6):
    month_number = randint(1, 12)

    year_number = randint(1924, 2004)

    print("guess", guess_number , name, "were you born in", month_number, "/", year_number, "?" ),
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do")
    else:
        print("Drat! Lemme try again!")
